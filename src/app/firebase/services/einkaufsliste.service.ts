import { AuthenticationService } from './authentication.service';
import { Observable } from 'rxjs';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
  DocumentChangeAction,
  DocumentReference,
} from '@angular/fire/compat/firestore';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class EinkaufslisteService {
  private einkauflisteCollection!: AngularFirestoreCollection<Listeneintrag>;

  constructor(
    private firestore: AngularFirestore,
    private authenticationService: AuthenticationService
  ) {
    this.setupFirebaseCollection();
    this.authenticationService.authStateChanged.subscribe(() =>
      this.setupFirebaseCollection()
    );
  }

  public get(docID: string): Promise<Listeneintrag> {
    return new Promise<Listeneintrag>((resolve, reject) => {
      this.getDocumentWithID(docID)
        .get()
        .subscribe((data) => {
          if (
            data.exists &&
            data.data()?.uid === this.authenticationService.getCurrentUserId()
          ) {
            resolve(data.data() as Listeneintrag);
          } else {
            reject('404 - Data is not existing');
          }
        });
    });
  }

  public getAll(): Observable<DocumentChangeAction<Listeneintrag>[]> {
    return this.einkauflisteCollection.snapshotChanges();
  }

  public create(
    eintrag: Listeneintrag
  ): Promise<DocumentReference<Listeneintrag>> {
    eintrag = this.addUserIdToEintrag(eintrag);
    return this.einkauflisteCollection.add(eintrag);
  }

  public delete(docID: string): Promise<void> {
    return this.getDocumentWithID(docID).delete();
  }

  public update(docID: string, eintrag: Listeneintrag): Promise<void> {
    eintrag = this.addUserIdToEintrag(eintrag);
    return this.getDocumentWithID(docID).update(eintrag);
  }

  private addUserIdToEintrag(eintrag: Listeneintrag): Listeneintrag {
    eintrag.uid = this.authenticationService.getCurrentUserId();
    return eintrag;
  }

  private setupFirebaseCollection(): void {
    this.einkauflisteCollection = this.firestore.collection(
      'einkaufsliste',
      (ref) => {
        return ref.where(
          'uid',
          '==',
          this.authenticationService.getCurrentUserId()
        );
        // .orderBy('anzahl')
      }
    );
  }

  private getDocumentWithID(
    docID: string
  ): AngularFirestoreDocument<Listeneintrag> {
    return this.einkauflisteCollection.doc(docID);
  }
}

export interface Listeneintrag {
  uid: string;
  artikel: string;
  anzahl: number;
  gekauft: boolean;
}
