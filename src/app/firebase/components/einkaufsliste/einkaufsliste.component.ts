import {
  EinkaufslisteService,
  Listeneintrag,
} from '../../services/einkaufsliste.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Table } from 'primeng/table';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DocumentChangeAction } from '@angular/fire/compat/firestore';

@Component({
  selector: 'app-einkaufsliste',
  templateUrl: './einkaufsliste.component.html',
  styleUrls: ['./einkaufsliste.component.scss'],
})
export class EinkaufslisteComponent implements OnInit {
  @ViewChild('dt') private datatable!: Table;

  public einkaufliste: DocumentChangeAction<Listeneintrag>[] = [];
  public selectedListe: DocumentChangeAction<Listeneintrag>[] = [];
  public eintragForEditOrNew: Listeneintrag = {
    uid: '',
    artikel: '',
    anzahl: 0,
    gekauft: false,
  };

  public dialogOpen = false;
  public dialogMode: DialogMode = DialogMode.create;
  public dialogEditDocId = '';

  public readonly dialogModeOptions = DialogMode;

  constructor(
    private einkaufslisteService: EinkaufslisteService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) {}

  public ngOnInit(): void {
    this.getListeneintraege();
  }

  public globalFilter(event: InputEvent): void {
    const value = (event.target as HTMLInputElement).value;
    this.datatable.filterGlobal(value, 'contains');
  }

  public deleteSelectedEintrageWithConfirm() {
    this.confirmationService.confirm({
      message:
        'Sind sie sicher, dass Sie die ausgewählten Listeneinträge löschen wollen?',
      header: 'Löschen?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Ja',
      rejectLabel: 'Nein',
      accept: () => {
        this.deleteListeneintraege(this.selectedListe);
        this.selectedListe = [];
      },
    });
  }

  public deleteListeneintragWithConfirm(
    eintrag: DocumentChangeAction<Listeneintrag>
  ) {
    this.confirmationService.confirm({
      message:
        'Sind sie sicher, dass Sie diesen Listeneinträge löschen wollen?',
      header: 'Löschen?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Ja',
      rejectLabel: 'Nein',
      accept: () => {
        this.deleteListeneintraege([eintrag]);
        this.selectedListe = [];
      },
    });
  }

  public openDialog(
    dialogMode: DialogMode,
    listeneintrag?: DocumentChangeAction<Listeneintrag>
  ): void {
    this.dialogMode = dialogMode;
    this.dialogOpen = true;

    if (dialogMode === DialogMode.edit && listeneintrag) {
      this.dialogEditDocId = listeneintrag.payload.doc.id;
      this.eintragForEditOrNew = { ...listeneintrag.payload.doc.data() };
    } else {
      this.eintragForEditOrNew = {
        uid: '',
        artikel: '',
        anzahl: 0,
        gekauft: false,
      };
    }
  }

  public dialogSavePressed(): void {
    this.dialogOpen = false;

    if (this.dialogMode === DialogMode.edit) {
      this.updateListeneintrag(this.dialogEditDocId, this.eintragForEditOrNew);
    } else if (this.dialogMode === DialogMode.create) {
      this.createListeneintrag(this.eintragForEditOrNew);
    }
  }

  public resetDialogField(): void {
    this.eintragForEditOrNew = {
      uid: '',
      artikel: '',
      anzahl: 0,
      gekauft: false,
    };
    this.dialogOpen = false;
  }

  /* ------------------------------
   * CRUD OPERATIONS
   * ------------------------------ */

  private createListeneintrag(eintrag: Listeneintrag): void {
    this.einkaufslisteService.create(eintrag);
  }

  private getListeneintraege(): void {
    this.einkaufslisteService
      .getAll()
      .subscribe((einkaufsliste) => (this.einkaufliste = einkaufsliste));
  }

  private updateListeneintrag(docID: string, eintrag: Listeneintrag): void {
    this.einkaufslisteService.update(docID, eintrag);
  }

  private deleteListeneintraege(
    eintraege: DocumentChangeAction<Listeneintrag>[]
  ): void {
    const promises: Promise<void>[] = [];

    for (const eintrag of eintraege) {
      promises.push(this.einkaufslisteService.delete(eintrag.payload.doc.id));
    }

    Promise.all(promises)
      .then(() =>
        this.messageService.add({
          severity: 'success',
          summary: 'Erfolgreich',
          detail: 'Listeneintrag gelöscht',
          life: 3000,
        })
      )
      .catch((error) => {
        this.messageService.add({
          severity: 'error',
          summary: 'Fehler',
          detail: 'Fehler beim löschen',
          life: 3000,
        });
        console.error(error);
      });
  }
}

enum DialogMode {
  edit = 'edit',
  create = 'create',
}
