import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { Component } from '@angular/core';

@Component({
  templateUrl: './email-login.component.html',
  styleUrls: ['./email-login.component.scss'],
})
export class EmailLoginComponent {
  public email = '';
  public password = '';

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {}

  public signup(): void {
    this.authenticationService
      .emailSignup(this.email, this.password)
      .then(() => this.login())
      .catch((error) =>
        this.authenticationService.handleGoogleAuthError(error)
      );
  }

  public login(): void {
    this.authenticationService
      .emailSignin(this.email, this.password)
      .then(() => this.router.navigate(['/']))
      .catch((error) =>
        this.authenticationService.handleGoogleAuthError(error)
      );
  }
}
