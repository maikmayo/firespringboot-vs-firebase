import {
  EinkaufslisteService,
  Listeneintrag,
} from '../../services/einkaufsliste.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Table } from 'primeng/table';
import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'app-einkaufslistesb',
  templateUrl: './einkaufsliste.component.html',
  styleUrls: ['./einkaufsliste.component.scss'],
})
export class EinkaufslisteSBComponent implements OnInit {
  @ViewChild('dt') private datatable!: Table;

  public einkaufliste: Listeneintrag[] = [];
  public selectedListe: Listeneintrag[] = [];
  public eintragForEditOrNew: Listeneintrag = {
    id: 0,
    artikel: '',
    anzahl: 0,
    gekauft: false,
  };

  public dialogOpen = false;
  public dialogMode: DialogMode = DialogMode.create;
  public dialogEditDocId = '';

  public readonly dialogModeOptions = DialogMode;

  constructor(
    private einkaufslisteService: EinkaufslisteService,
    private confirmationService: ConfirmationService
  ) {}

  public ngOnInit(): void {
    this.getListeneintraege();
  }

  public globalFilter(event: InputEvent): void {
    const value = (event.target as HTMLInputElement).value;
    this.datatable.filterGlobal(value, 'contains');
  }

  public deleteSelectedEintrageWithConfirm() {
    this.confirmationService.confirm({
      message:
        'Sind sie sicher, dass Sie die ausgewählten Listeneinträge löschen wollen?',
      header: 'Löschen?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Ja',
      rejectLabel: 'Nein',
      accept: () => {
        this.deleteListeneintraege(this.selectedListe);
        this.selectedListe = [];
      },
    });
  }

  public deleteListeneintragWithConfirm(eintrag: Listeneintrag) {
    this.confirmationService.confirm({
      message:
        'Sind sie sicher, dass Sie diesen Listeneinträge löschen wollen?',
      header: 'Löschen?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Ja',
      rejectLabel: 'Nein',
      accept: () => {
        this.deleteListeneintraege([eintrag]);
        this.selectedListe = [];
      },
    });
  }

  public openDialog(
    dialogMode: DialogMode,
    listeneintrag?: Listeneintrag
  ): void {
    this.dialogMode = dialogMode;
    this.dialogOpen = true;

    if (dialogMode === DialogMode.edit && listeneintrag) {
      this.eintragForEditOrNew = { ...listeneintrag };
    } else {
      this.eintragForEditOrNew = {
        id: 0,
        artikel: '',
        anzahl: 0,
        gekauft: false,
      };
    }
  }

  public dialogSavePressed(): void {
    this.dialogOpen = false;

    if (this.dialogMode === DialogMode.edit) {
      console.log('update');

      this.updateListeneintrag(this.eintragForEditOrNew);
    } else if (this.dialogMode === DialogMode.create) {
      this.createListeneintrag(this.eintragForEditOrNew);
    }
  }

  public resetDialogField(): void {
    this.eintragForEditOrNew = {
      id: 0,
      artikel: '',
      anzahl: 0,
      gekauft: false,
    };
    this.dialogOpen = false;
  }

  /* ------------------------------
   * CRUD OPERATIONS
   * ------------------------------ */

  private createListeneintrag(eintrag: Listeneintrag): void {
    this.einkaufslisteService
      .create(eintrag)
      .subscribe((eintrag: Listeneintrag) => this.einkaufliste.push(eintrag));
  }

  private updateListeneintrag(eintrag: Listeneintrag): void {
    this.einkaufslisteService
      .update(eintrag)
      .subscribe((eintrag: Listeneintrag) => this.einkaufliste.push(eintrag));
  }

  private getListeneintraege(): void {
    this.einkaufslisteService.getAll().subscribe((einkaufsliste) => {
      console.log(einkaufsliste);
      this.einkaufliste = einkaufsliste;
    });
  }

  private deleteListeneintraege(eintraege: Listeneintrag[]): void {
    for (const eintrag of eintraege) {
      this.einkaufslisteService.delete(eintrag.id).subscribe(() => {
        this.einkaufliste.filter((val) => val.id !== eintrag.id);
      });
    }
  }
}

enum DialogMode {
  edit = 'edit',
  create = 'create',
}
