import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.scss'],
})
export class MainpageSBComponent {
  public isLogin = false;
  public username = '';

  constructor(private router: Router) {}

  public navigateToEmailLogin(): void {
    this.router.navigate(['email-login']);
  }
}
