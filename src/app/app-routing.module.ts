import { MainpageSBComponent } from './springboot/components/mainpage/mainpage.component';
import { MainpageComponent } from './firebase/components/mainpage/mainpage.component';
import { EmailLoginComponent } from './firebase/components/email-login/email-login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'email-login', component: EmailLoginComponent },
  { path: 'springboot', component: MainpageSBComponent },
  { path: 'firebase', component: MainpageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
